Changelog
=========

v0.2.1 (2022-02-18)
-------------------

### New

- Support for OMNI2 missing values


v0.2.0 (2022-02-13)
-------------------

### New

- Support for OMNI2 (extended) 1-hourly text files from <https://omniweb.gsfc.nasa.gov/ow.html>
  with tests and documentation

### Changes

- Restructuring and renaming of the internal (sub)modules
- Other documentation updates


v0.1.1 (2022-01-25)
-------------------

### Changes

- Updated data files
- Fixed, updated, and improved tests to increase code coverage
- Uses Github actions for CI and CD


v0.1.0 (2021-09-20)
-------------------

First official beta release.
